import type { Preview, App } from "@storybook/vue3";
import store from '../src/store';
import router from '../src/router';
import { setup } from '@storybook/vue3';

import "../src/assets/styles/storybook/_styles.scss";
import "../src/assets/styles/base/_helper.scss";
import "../src/assets/styles/_styles.scss";

import "@iconify/iconify/dist/iconify.min.js";
import "../src/assets/resources/fonts/line-icons-pro/line-icons-pro.css";

import { LoadPlugin } from "../src/plugins/common/cmp-overload.plugin";


setup((app: App) => {
  app.use(store).use(router).use(LoadPlugin);
})

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
};

export default preview;
