@Library('shared-library-sreasons@1.1')
import com.sreasons.PipelineUtil

def utils = new PipelineUtil(steps, this)

node
{
  def parameters = []

  stage("Setting Variables")
  {
    parameters = [
      projectID : "managementweb",
    ]
  }

  stage("Git") 
  {
    utils.initialize(parameters)
  }
  
  stage("Quality-Code"){
//    utils.executeSonnarForNode()
  }

  stage("Build") 
  {
    utils.executeBuildNodeJSApp()
  }

  stage("Release"){
    utils.executeUploadArtifact()
  }

  stage("Deploy"){
    utils.executeDeployWebApplication()
  }
}
