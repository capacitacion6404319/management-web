export type SubNavId =
    | "dashboard"
    | "layouts"
    | "elements"
    | "components"
    | "search";