
export interface RequestOptions {
    headers?: Record<string, string>;
  }
export interface HttpRestPort {
    get<T>(path: string, params: unknown): Promise<T[]>;
    post<T,E>(path: string, entity: T,options?:RequestOptions): Promise<E>;
    delete<T>(path: string): Promise<T>;
    put<T>(path: string, entity: T): Promise<T>;
}
