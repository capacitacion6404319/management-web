export interface RulesEnginePort {
    executeRule(ruleName: string, params: unknown): Promise<any>;
}