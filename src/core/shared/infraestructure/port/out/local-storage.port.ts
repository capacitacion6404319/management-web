export interface LocalStoragePort<T>{
    guardarToken(token: T):void;
    limpiarToken():void;
    obtenerToken():T|null;
}