import axios from "axios";

import { HttpRestPort,RequestOptions } from "../../port/out/http-rest.port";
import { HOSTSERVER } from "./constant/hostserver/hostserver.constant";
import { SingletonBase } from "../../../../shared/domain/singleton-base";

export const instance = axios.create({
    baseURL: HOSTSERVER,
});

instance.interceptors.request.use(
    (config) => {
        return config;
    },
    (err) => {
        Promise.reject(err);
    }
);

instance.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (error) => {
        console.log(error?.response?.data)
        console.log(error?.response?.status)
        return Promise.reject(error);
    }
);

export class AxiosRest extends SingletonBase implements HttpRestPort {
    get<T>(path: string, params: unknown): Promise<T[]> {
        return instance.get(path, {params});
    }
    post<T,E>(path: string, entity: T,options?:RequestOptions): Promise<E> {
        return instance.post(path, entity,options);
    }
    delete<T>(path: string): Promise<T> {
        return instance.delete(path);
    }
    put<T,E>(path: string, entity: T,options?:RequestOptions): Promise<E> {
        return instance.put(path, entity);
    }
}
