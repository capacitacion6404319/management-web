import { SingletonBase } from "../../../../../core/shared/domain/singleton-base";
import { RulesEnginePort } from "../../port/out/rules-engine.port";
import { Engine } from "json-rules-engine";

import { PATH_RULES } from "./constant/hostserver/hostserver.constant";
import { ResultRuleTypeEnum } from "./enum/result-rule-type.enum";

export class JsonEngineRules extends SingletonBase implements RulesEnginePort {
    async executeRule(ruleName: string, params: Record<string, any>) {
        const ruleResp = await fetch(`${PATH_RULES}/${ruleName}.json`);
        const rule = await ruleResp.json();

        const engine = new Engine(rule.decisions);

        return engine.run(params).then(
            (results) => {
                if (results?.events?.length > 0) {
                    results?.events?.forEach((value, index) => {                        
                        const item: any = results.events[index];
                        if (item?.type === ResultRuleTypeEnum.FUNCTION && item?.params) {
                            const fn = Function(
                                item.params.in.variableIn,
                                item.params.in.functionDef
                            );
                            item.params.out = fn(params);
                        }
                    });
                }
                console.log("result", results.events);
                return results.events;
            },
            (error) => {
                console.log(error);
            }
        );
    }
}