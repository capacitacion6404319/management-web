import {LocalStoragePort} from "../../../../../core/shared/infraestructure/port/out/local-storage.port";

export class LocalStorageAdapter<T> implements LocalStoragePort<T>{
    private key:string;

    constructor(key:string) {
        this.key=key;
    }
    guardarToken(token:T): void {
        const tokenStr = JSON.stringify(token);
        localStorage.setItem(this.key,tokenStr);
    }

    limpiarToken(): void {
        localStorage.removeItem(this.key);
    }

    obtenerToken(): T | null {
        const tokenStr = localStorage.getItem(this.key);
        if(tokenStr){
            return JSON.parse(tokenStr)as T;
        }
        return null;
    }


}