export class SingletonBase {
  private static instance: any;

  static getInstance<T>(clazz: new () => T): T {
    if (!this.instance || !(this.instance instanceof clazz)) {
      this.instance = new clazz();
    }
    return this.instance;
  }
}