export class CalcRules {

    amountUnitPerItem: number;
    valueUnitPerItem: number;
    amountBaseOfCharge: number;
    chargeFactor: number;
    priceUnitPerItem: number;

    constructor() {
        this.amountUnitPerItem = 12;
        this.valueUnitPerItem = 56;
        this.amountBaseOfCharge = 12;
        this.chargeFactor = 45;
        this.priceUnitPerItem = 12;

        const resul = this.calculateValueSalePerItem2(false, true, 
            this.valueUnitPerItem, this.amountUnitPerItem, 
            this.amountBaseOfCharge, this.chargeFactor, this.priceUnitPerItem);
        console.log(resul);
    }

    calculateValueSalePerItem(isFree: boolean, affectsTaxBase: boolean) {
        if (!isFree) {
            return this.valueUnitPerItem * 
            this.amountUnitPerItem - 
            this.calculateDiscountAmountPerItem(affectsTaxBase) + 
            this.calculateChargeAmountPerItem(affectsTaxBase);
        } else {
            return "otro flujo"
        }
    }

    calculateValueSalePerItem2(isFree: boolean, 
        affectsTaxBase: boolean,
        valueUnitPerItem: number,
        amountUnitPerItem: number,
        amountBaseOfCharge: number,
        chargeFactor: number,
        priceUnitPerItem: number) {
        
        //let valueCalculateDiscountAmountPerItem;
        //let valueCalculateChargeAmountPerItem;

        if (!isFree) {
            if (affectsTaxBase) {
                //valueCalculateDiscountAmountPerItem = amountBaseOfCharge * chargeFactor;
                //valueCalculateChargeAmountPerItem = amountBaseOfCharge * chargeFactor;

                return valueUnitPerItem * 
                    amountUnitPerItem - 
                    (amountBaseOfCharge * chargeFactor) + 
                    (amountBaseOfCharge * chargeFactor);
            } else if(!affectsTaxBase) {
                //valueCalculateDiscountAmountPerItem = amountBaseOfCharge * chargeFactor;
                //valueCalculateChargeAmountPerItem = amountBaseOfCharge * chargeFactor;

                return valueUnitPerItem * 
                amountUnitPerItem - 
                (amountBaseOfCharge * chargeFactor) + 
                (amountBaseOfCharge * chargeFactor);
            }
        } else if(isFree) {
            if (affectsTaxBase) {
                //valueCalculateDiscountAmountPerItem = amountBaseOfCharge * chargeFactor;
                //valueCalculateChargeAmountPerItem = amountBaseOfCharge * chargeFactor;

                return priceUnitPerItem * 
                amountUnitPerItem - 
                (amountBaseOfCharge * chargeFactor) + 
                (amountBaseOfCharge * chargeFactor);
            } else if(!affectsTaxBase) {
                //valueCalculateDiscountAmountPerItem = amountBaseOfCharge * chargeFactor;
                //valueCalculateChargeAmountPerItem = amountBaseOfCharge * chargeFactor;

                return priceUnitPerItem * 
                amountUnitPerItem - 
                (amountBaseOfCharge * chargeFactor) + 
                (amountBaseOfCharge * chargeFactor);
            }
        }
    }

    calculateDiscountAmountPerItem(affectsTaxBase: boolean) {
        if (affectsTaxBase) {
            return this.amountBaseOfCharge * this.chargeFactor;
        } else  {
            return this.amountBaseOfCharge * this.chargeFactor;
        }
    }

    calculateChargeAmountPerItem(affectsTaxBase: boolean) {
        if (affectsTaxBase) {
            return this.amountBaseOfCharge * this.chargeFactor;
        } else {
            return this.amountBaseOfCharge * this.chargeFactor;
        }
    }
}

const obj = new CalcRules();
