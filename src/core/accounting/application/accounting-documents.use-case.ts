import { SingletonBase } from "../../shared/domain/singleton-base";
import { AccountingDocumentsPort } from "../infraestructure/port/in/accounting-documents.port";

import { RulesEnginePort } from "../../shared/infraestructure/port/out/rules-engine.port";
import { JsonEngineRules } from "../../shared/infraestructure/adapter/out/json-engine-rules.adapter";

export class AccountingDocumentsUseCase extends SingletonBase implements AccountingDocumentsPort {
    private rulesEnginePort: RulesEnginePort;

    constructor() {
        super();
        this.rulesEnginePort = JsonEngineRules.getInstance(JsonEngineRules);
    }
    async calculateValueSalePerItem(ruleName: string, params: any): Promise<number> {
        const respRule = await this.rulesEnginePort.executeRule(ruleName, params);
        return respRule?.length > 0 ? respRule[0].params.out : 0;
    }

    async getDocumentState(ruleName: string, params: any): Promise<boolean> {
        const respRule = await this.rulesEnginePort.executeRule(ruleName, params);
        return respRule?.length > 0 ? respRule[0].params.out : false;
    }
}