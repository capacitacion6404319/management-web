import { AccountingDocumentsUseCase } from "../../../application/accounting-documents.use-case";
import { AccountingDocumentsPort } from "../../port/in/accounting-documents.port";

export const getDocumentState = async (ruleName: string, params: any) => {
    const accountingDocumentsPort: AccountingDocumentsPort = AccountingDocumentsUseCase.getInstance(AccountingDocumentsUseCase);
    return await accountingDocumentsPort.getDocumentState(ruleName, params);
}

export const calculateValueSalePerItem = async (ruleName: string, params: any) => {
    const accountingDocumentsPort: AccountingDocumentsPort = AccountingDocumentsUseCase.getInstance(AccountingDocumentsUseCase);
    return await accountingDocumentsPort.calculateValueSalePerItem(ruleName, params);
}