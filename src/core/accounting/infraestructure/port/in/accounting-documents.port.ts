export interface AccountingDocumentsPort {
    getDocumentState(nameRule: string, params: any): Promise<boolean>;
    calculateValueSalePerItem(nameRule: string, params: any): Promise<number>;
}