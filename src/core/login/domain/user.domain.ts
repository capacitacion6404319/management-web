export class UserLogin{
    Username?: string;
    Password?: string;
}
export class User {
    IdCuenta?: number;
    IdUsuario?: number;
    Username?: string;
    IdEmpresa?: number;
}
export class UserToken {
    AccessToken?: string;
    RefreshToken?:string;
    ExpiresIn?:number;
    TimeLogin?:number;
    TipoToken?:number;
}
