import { LoginPort } from "../../../port/in/login.port";
import { User } from "../../../../domain/user.domain";
import { LoginUseCase } from "../../../../application/use-case/login.use-case";

export const login = (user: User) => {
    const loginPort: LoginPort = LoginUseCase.getInstance(LoginUseCase);
    return loginPort.login(user);
}