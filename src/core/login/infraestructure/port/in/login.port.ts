import { User } from "../../../domain/user.domain";

export interface LoginPort {
    login(user: User): Promise<User>;
}