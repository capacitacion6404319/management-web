import { LoginPort } from "../../infraestructure/port/in/login.port";
import {User, UserLogin, UserToken} from "../../domain/user.domain";
import {LoginRequest} from "../../domain/login.domain";
import { HttpRestPort,RequestOptions } from "../../../shared/infraestructure/port/out/http-rest.port";
import { AxiosRest } from "../../../shared/infraestructure/adapter/out/axios-rest.adapter";
import { LocalStorageAdapter} from "../../../shared/infraestructure/adapter/out/local-storage.adapter";
import { SingletonBase } from "../../../shared/domain/singleton-base";
import { MS_SEGURIDAD} from "../../../shared/infraestructure/adapter/out/constant/hostserver/hostserver.constant"
import { ID_EMPRESA } from "../../../shared/infraestructure/adapter/out/constant/login/login.constant";


export class LoginUseCase extends SingletonBase implements LoginPort {
    private restPort: HttpRestPort;
    private path:string;
    private localStorageAdapter: LocalStorageAdapter<UserToken>;
    constructor() {
        super();
        this.localStorageAdapter = new LocalStorageAdapter<UserToken>("authKey");
        this.restPort = AxiosRest.getInstance(AxiosRest);
        this.path = MS_SEGURIDAD+"/api/v3";
    }
    async login(userLogin: UserLogin): Promise<User> {
        const userToken = await this.authenticateUser(userLogin);
        if (!userToken.AccessToken) {
            throw new Error("Authentication failed: No access token received.");
        }

        // Decodifica la información de la cuenta del primer token
        const accountInfo = this.decodeAccountFromToken(userToken.AccessToken);
        if (!accountInfo.IdCuenta) {
            throw new Error("IdCuenta is missing in the token payload.");
        }

        // Realiza la segunda parte del proceso de login y obtiene el token del usuario
        const fullUserToken = await this.loginWithCompany(accountInfo.IdCuenta, userToken.AccessToken);
        if (!fullUserToken.AccessToken) {
            throw new Error("Failed to login with company: No access token received.");
        }

        // Decodifica la información del usuario del segundo token
        const userInfo = this.decodeUserFromToken(fullUserToken.AccessToken);
        this.localStorageAdapter.guardarToken(fullUserToken);

        // Construye el objeto User con toda la información necesaria
        const user = new User();
        user.IdCuenta = accountInfo.IdCuenta;
        user.IdEmpresa = ID_EMPRESA;
        user.IdUsuario = userInfo.IdUsuario;
        user.Username = userInfo.Username;

        return user;
    }

    private decodeAccountFromToken(token: string): any {
        const decoded = JSON.parse(atob(token.split('.')[1]));
        return {
            IdCuenta: decoded.scopes?.IdCuenta
        };
    }

    private decodeUserFromToken(token: string): any {
        const decoded = JSON.parse(atob(token.split('.')[1]));
        return {
            IdUsuario: decoded.scopes?.IdUsuario,
            Username: decoded.scopes?.Usuario
        };
    }

    private async loginWithCompany(idCuenta: number, accessToken: string): Promise<UserToken> {
        const companyLoginPath = `${this.path}/usuarios/universales/${ID_EMPRESA}/login`;
        const companyLoginHeaders: RequestOptions = { headers: { 'Authorization': `Bearer ${accessToken}` } };
        return this.restPort.post<LoginRequest, UserToken>(companyLoginPath, { Id: idCuenta }, companyLoginHeaders);
    }
    private async authenticateUser(userLogin :UserLogin):Promise<UserToken>{
        const credentials =btoa(`${userLogin.Username}:${userLogin.Password}`);
        const headers: RequestOptions ={headers:{'Authorization':`Basic ${credentials}`}}
        // se obtiene el token del usuario
        return this.restPort.post<UserLogin,UserToken>(this.path + "/usuarios/universales/cuenta/login", {}, headers);

    }
}
