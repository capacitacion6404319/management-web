export enum TypeHeaderSubNavsEnum {
    DASHBOARD = "dashboard",
    LAYOUTS = "layouts",
    ELEMENTS = "elements",
    COMPONENTS = "components",
    SEARCH = "search"
}