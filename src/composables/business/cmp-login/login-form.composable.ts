import { computed } from "vue";
import { useStore } from "vuex";

import { executeAction } from "../../../utils/vuex.util";

import {
    LOGIN_FORM_STORE_GET_USER,
    LOGIN_FORM_STORE_IS_USER_INVALID,
    LOGIN_FORM_STORE_LOGIN_USER,
    LOGIN_FORM_STORE_RESET_VALUES,
    LOGIN_FORM_STORE_CHECK_SESSION,
    LOGIN_FORM_STORE_IS_TOKEN_VALID
} from "../../../store/components/business/cmp-login/login-form.store";
import {UserLogin} from "../../../core/login/domain/user.domain";

export default function useLoginForm() {
    const store = useStore();

    const loginUser = async (user: UserLogin) => {
        executeAction(LOGIN_FORM_STORE_LOGIN_USER, user);
    }

    const checkSession = async ()=>{
        executeAction(LOGIN_FORM_STORE_CHECK_SESSION,null);
    }

    const isUserInvalid = () => {
        const isUserInvalid = computed(
            () => store.getters[LOGIN_FORM_STORE_IS_USER_INVALID]
        );
        return isUserInvalid;
    }
    const isTokenValid = () => {
        const isTokenValid = computed(
            () => store.getters[LOGIN_FORM_STORE_IS_TOKEN_VALID]
        );
        return isTokenValid;
    }

    const getUser = () => {
        const user = computed(
            () => store.getters[LOGIN_FORM_STORE_GET_USER]
        );
        return user;
    }

    const resetValues = async () => {
        executeAction(LOGIN_FORM_STORE_RESET_VALUES, null);
    }

    return {
        getUser,
        isUserInvalid,
        loginUser,
        resetValues,
        checkSession,
        isTokenValid,
    }
}