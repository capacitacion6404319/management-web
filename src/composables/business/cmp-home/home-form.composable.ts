import { computed } from "vue";
import { useStore } from "vuex";

import { executeAction } from "../../../utils/vuex.util";

import {
    HOME_FORM_STORE_GET_DOCUMENT_STATE,
    HOME_FORM_STORE_SET_DOCUMENT_STATE,
    HOME_FORM_STORE_GET_VALUE_SALE_PER_ITEM,
    HOME_FORM_STORE_SET_VALUE_SALE_PER_ITEM,
    HOME_FORM_STORE_RESET_VALUES
} from "../../../store/components/business/cmp-home/home-form.store";

export default function useLoginForm() {
    const store = useStore();

    const setDocumentState = (params: any) => {
        executeAction(HOME_FORM_STORE_SET_DOCUMENT_STATE, params);
    }

    const setValueSalePerItem = (params: any) => {
        executeAction(HOME_FORM_STORE_SET_VALUE_SALE_PER_ITEM, params);
    }

    const getDocumentState = () => {
        const documentState = computed(
            () => store.getters[HOME_FORM_STORE_GET_DOCUMENT_STATE]
        );
        return documentState;
    }

    const getValueSalePerItem = () => {
        const valueSalePerItem = computed(
            () => store.getters[HOME_FORM_STORE_GET_VALUE_SALE_PER_ITEM]
        );
        return valueSalePerItem;
    }

    const resetValues = () => {
        executeAction(HOME_FORM_STORE_RESET_VALUES, null);
    }

    return {
        setDocumentState,
        getDocumentState,
        setValueSalePerItem,
        getValueSalePerItem,
        resetValues
    }
}