import { h, render } from 'vue';

import CmpOverload from "../../../components/common/cmp-overload/cmp-overload.vue";

export interface Props {
    container?: HTMLElement,
}

export default function useCmpOverload(globalProps:Props = {}) {
    const show = (props = globalProps) => {
        let container = props.container;
        if (!props.container) {
            container = document.body;
        }
        const instance = createComponent(CmpOverload, props, container);
    };
    const hide = (props = globalProps) => {
        let parent = props.container as HTMLElement;
        if (!props.container) {
            parent = document.body;
            parent.classList.remove("v-overload");
        }
        const child = parent!.lastChild as Element;
        render(null, child);
        removeElement(child);
    };
    const createComponent = (component: any, props: any, containerParent: any) => {
        const vNode = h(component, props);
        const container = document.createElement('div');
        containerParent.classList.add('v-overload');
        containerParent.appendChild(container);
        render(vNode, container);
        return vNode.component;
    };
    return {
        show,
        hide,
    };
}

export function removeElement(el: any) {
    if (typeof el.remove !== 'undefined') {
        el.remove();
    }
    else {
        el.parentNode.removeChild(el);
    }
}