import { computed } from "vue";
import { useStore } from "vuex";

import { executeAction } from "../../utils/vuex.util";

import { changeThemeApp } from "../../themes/app.theme";
import { changeThemeHeader } from "../../themes/common/header.theme";
import { changeThemeOptionDropdown } from "../../themes/common/option-dropdown.theme";

import {
    THEME_STORE_GET_TYPE,
    THEME_STORE_SET_TYPE
} from "../../store/components/common/theme.store";

export default function useTheme() {

    const store = useStore();

    const type = computed(
        () => store.getters[THEME_STORE_GET_TYPE]
    );

    const setType = (type: number) => {
        executeAction(THEME_STORE_SET_TYPE, type);
        changeTheme(type);
    }

    const changeTheme = (type: number) => {
        changeThemeApp(type);
        changeThemeHeader(type);
        changeThemeOptionDropdown(type);
    }

    return {
        type,
        changeTheme,
        setType
    }
}