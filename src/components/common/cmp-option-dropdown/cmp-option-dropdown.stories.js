import CmpOptionDropdown from "./cmp-option-dropdown.vue";

import { PLANTILLA_HTML } from "../../../constants/storybook.constant";

const TITULO = `CMP-OPTION-DROPDOWN`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> renderiza componente que despliega un menú, en él se definen las siguientes <strong>propiedades</strong>:`;

const PROPS = `
    <li><strong>fill</strong>: Colorea el contenedor de la opción</li>
    <li><strong>hasNotification</strong>: Flag que indica si el option es o tiene notificación</li>
`;

const COMPONENT_BASE = `
<div style="width: 500px; margin-top: 20px;">
    <cmp-option-dropdown v-bind="args">      
        <template #icon>
            <i aria-hidden='true' class='iconify' data-icon='feather:bell'></i>
        </template>
    </cmp-option-dropdown>
</div>`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-01-30][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'Components/Common/CmpOptionDropdown',
    component: CmpOptionDropdown,
}

const TemplateBase = args => ({
    components: { CmpOptionDropdown },
    setup() {
        return { args }
    },
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT_BASE}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const Base = TemplateBase.bind({});
Base.args = {
    'hasNotification': true
}