import { defineComponent } from "vue";

export default defineComponent({
    setup() {
        const PREFIX = "cmp-dropdown-menu";

        return {
            PREFIX
        };
    },
});