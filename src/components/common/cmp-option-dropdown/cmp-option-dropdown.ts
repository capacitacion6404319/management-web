import { defineAsyncComponent, defineComponent, onMounted, onUnmounted, ref, watchEffect } from "vue";

export default defineComponent({
    components: {
        CmpDropdownMenu: defineAsyncComponent(
            () => import('./cmp-dropdown-menu/cmp-dropdown-menu.vue')
        )
    },
    props: {
        fill: {
            type: Boolean,
            default: false
        },
        hasNotification: {
            type: Boolean,
            default: false
        }
    },
    setup() {
        const PREFIX = "cmp-option-dropdown";

        const cmpOptionDropdown = ref();

        const isSelect = ref(false);

        const select = () => {
            isSelect.value = !isSelect.value;
        }

        const close = () => {
            isSelect.value = false;
        }

        const handleClickOutside = (event: any) => {
            if (isSelect.value && cmpOptionDropdown.value && !cmpOptionDropdown.value.contains(event.target)) {
                close();
            }
        };

        watchEffect(() => {
            cmpOptionDropdown.value;
        })

        onMounted(() => {
            document.addEventListener('click', handleClickOutside);
        })

        onUnmounted(() => {
            document.removeEventListener('click', handleClickOutside);
        })

        return {
            PREFIX,
            isSelect,
            cmpOptionDropdown,
            select,
            close
        }
    }
})