import CmpOverload from "./cmp-overload.vue";

export default {
    title: 'Components/Common/CmpOverload',
    component: CmpOverload,
}

const TemplateBase = args => ({
    components: { CmpOverload },
    setup() {
        return { args }
    },
    template: `<div style="height: 100vh; background: #959595"><cmp-overload v-bind="args"></cmp-overload></div>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}