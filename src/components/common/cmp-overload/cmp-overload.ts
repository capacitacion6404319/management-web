import { defineComponent } from "vue";

export default defineComponent({
  props: {
    container: {
      type: [Object, Function, HTMLElement],
      default: null,
    },
  },
  setup() {
    const PREFIX = "v-overload";
    return {
        PREFIX
    }
  }
});
