import { defineComponent } from "vue";

export default defineComponent({
    setup() {
        const PREFIX = "cmp-sub-navs";

        return {
            PREFIX
        }
    }
})