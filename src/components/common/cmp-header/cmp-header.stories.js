import CmpHeader from "./cmp-header.vue";

export default {
    title: 'Components/Common/CmpHeader',
    component: CmpHeader,
}

const TemplateBase = args => ({
    components: { CmpHeader },
    setup() {
        return { args }
    },
    template: `<cmp-header v-bind="args"></cmp-header>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}