import { defineAsyncComponent, defineComponent } from "vue";

export default defineComponent({
    components: {
        CmpHeaderSummary: defineAsyncComponent(
            () => import(/* webpackChunkName: "cmp-header" */ "./cmp-header-summary/cmp-header-summary.vue")
        ),
        CmpHeaderNavs: defineAsyncComponent(
            () => import(/* webpackChunkName: "cmp-header" */ "./cmp-header-navs/cmp-header-navs.vue")
        ),
        CmpHeaderOptions: defineAsyncComponent(
            () => import(/* webpackChunkName: "cmp-header" */ "./cmp-header-options/cmp-header-options.vue")
        )
    },
    setup() {
        const PREFIX = "cmp-header"

        return {
            PREFIX
        }
    }
})