import CmpHeaderNavs from "./cmp-header-navs.vue";

import { PLANTILLA_HTML } from "../../../../constants/storybook.constant";

const TITULO = `CMP-HEADER-NAVS`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> renderiza la barra de navegación del aplicativo, en él se definen las siguientes <strong>propiedades</strong>:`;

const PROPS = `

`;

const COMPONENT_BASE = `
<div style="width: 100px; margin-top: 20px;">
    <cmp-header-navs v-bind="args"></cmp-header-navs>
</div>`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-01-30][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'Components/Common/CmpHeader/CmpHeaderNavs',
    component: CmpHeaderNavs,
}

const TemplateBase = args => ({
    components: { CmpHeaderNavs },
    setup() {
        return { args }
    },
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT_BASE}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const Base = TemplateBase.bind({});
Base.args = {

}