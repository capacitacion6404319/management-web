import { defineComponent } from "vue";

export default defineComponent({
    props: {
        title: {
            type: String,
            default: ""
        }
    },
    setup() {
        const PREFIX = "cmp-header-nav";

        return {
            PREFIX
        }
    }
})