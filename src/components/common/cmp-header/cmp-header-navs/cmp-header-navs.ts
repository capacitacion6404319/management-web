import { defineAsyncComponent, defineComponent, ref } from "vue";

import { SubNavId } from "../../../../types/app.type";

export default defineComponent({
    components: {
        CmpHeaderLogo: defineAsyncComponent(
            () => import("../cmp-header-logo/cmp-header-logo.vue")
        ),
        CmpHeaderNav: defineAsyncComponent(
            () => import("./cmp-header-nav/cmp-header-nav.vue")
        ),
        CmpHeaderSubNavs: defineAsyncComponent(
            () => import("./cmp-header-sub-navs/cmp-header-sub-navs.vue")
        )
    },
    setup() {
        const PREFIX = "cmp-header-options";

        const navSelect = ref<SubNavId>();

        const select = (type: SubNavId) => {
            if (navSelect.value === type) {
                navSelect.value = undefined;
            } else {
                navSelect.value = type
            }            
        }

        return {
            PREFIX,
            select,
            navSelect
        }
    }
})