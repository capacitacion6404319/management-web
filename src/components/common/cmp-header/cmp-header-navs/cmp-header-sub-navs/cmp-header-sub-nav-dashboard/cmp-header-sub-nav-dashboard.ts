import { defineComponent, ref } from "vue";

import CmpSubNavs from "../../../../cmp-sub-navs/cmp-sub-navs.vue";

export default defineComponent({
    components: {
        CmpSubNavs
    },
    setup() {
        const PREFIX = "cmp-header-sub-nav-dashboard";
        const activeTab = ref("dashboards");

        const select = (type: string) => {
            activeTab.value = type;
        }

        return {
            PREFIX,
            activeTab,
            select
        }
    }
})