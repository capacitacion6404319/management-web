import { defineComponent, PropType, shallowRef, watchEffect } from "vue";

import cmpHeaderSubNavDashboard from "./cmp-header-sub-nav-dashboard/cmp-header-sub-nav-dashboard.vue";

import { TypeHeaderSubNavsEnum } from "../../../../../enums/type-subnavs.enum";

import { SubNavId } from "../../../../../types/app.type";

export default defineComponent({
    props: {
        navSelect: {
            type: String as PropType<SubNavId>,
            default: ""
        }
    },
    setup(props) {
        const PREFIX = "cmp-header-sub-navs";
        const subNavComponent = shallowRef();
        
        const chooseSubNavComponent = () => {
            if (props.navSelect as string === TypeHeaderSubNavsEnum.DASHBOARD) {
                subNavComponent.value = cmpHeaderSubNavDashboard;
            } else {
                subNavComponent.value = null;
            }
        }

        watchEffect(() => {
            props.navSelect;
            chooseSubNavComponent();
        })

        return {
            PREFIX,
            subNavComponent
        }
    }
})