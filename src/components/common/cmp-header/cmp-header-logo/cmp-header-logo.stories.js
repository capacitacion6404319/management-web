import CmpHeaderLogo from "./cmp-header-logo.vue";

import { PLANTILLA_HTML } from "../../../../constants/storybook.constant";

const TITULO = `CMP-HEADER-LOGO`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> renderiza el logo del aplicativo, en él se definen las siguientes <strong>propiedades</strong>:`;

const PROPS = `

`;

const COMPONENT_BASE = `
<div style="width: 100px; margin-top: 20px;">
    <div style="border: 1px solid whitesmoke"><cmp-header-logo v-bind="args"></cmp-header-logo></div>
</div>`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-01-30][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'Components/Common/CmpHeader/CmpHeaderLogo',
    component: CmpHeaderLogo,
}

const TemplateBase = args => ({
    components: { CmpHeaderLogo },
    setup() {
        return { args }
    },
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT_BASE}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const Base = TemplateBase.bind({});
Base.args = {

}