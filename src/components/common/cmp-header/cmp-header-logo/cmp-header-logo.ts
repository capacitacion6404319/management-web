import { defineComponent, ref, watchEffect } from "vue";

import useTheme from "../../../../composables/common/theme.composable";

import { TypeThemeEnum } from "../../../../enums/type-theme.enum";

import { PATH_RESOURCES } from "../../../../constants/app.constant";

export default defineComponent({
    setup() {
        const PREFIX = "cmp-header-logo";
        const icon = ref("logo");

        const { type } = useTheme();

        watchEffect(() => {
            icon.value;
            if (type.value === TypeThemeEnum.LIGHT) {
                icon.value = "logo";
            } else if (type.value === TypeThemeEnum.DARK) {
                icon.value = "logo-dark";
            }
        });

        return {
            PREFIX,
            PATH_RESOURCES,
            icon
        }
    }
})