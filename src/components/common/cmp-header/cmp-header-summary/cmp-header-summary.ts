import { defineAsyncComponent, defineComponent, ref } from "vue";

import { translate } from "../../../../utils/language.util";

import { PATH_RESOURCES } from "../../../../constants/app.constant";

export default defineComponent({
    components: {
        CmpHeaderLogo: defineAsyncComponent(
            () => import("../cmp-header-logo/cmp-header-logo.vue")
        ),
    },
    setup() {
        const PREFIX = "cmp-header-summary";
        const isMenuOpen = ref(false);

        const openCloseMenu = () => {
            isMenuOpen.value = !isMenuOpen.value;
        };

        return {
            PREFIX,
            PATH_RESOURCES,
            isMenuOpen,
            openCloseMenu,
            translate,
        };
    },
});
