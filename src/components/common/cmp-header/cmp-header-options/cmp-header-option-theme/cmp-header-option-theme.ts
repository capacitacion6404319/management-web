import { defineComponent, ref } from "vue";

import useTheme from "../../../../../composables/common/theme.composable";

import { TypeThemeEnum } from "../../../../../enums/type-theme.enum";

export default defineComponent({
    setup() {
        const PREFIX = "cmp-header-option-theme";

        const isDark = ref(false);

        const { setType } = useTheme();

        const change = (event: any) => {
            isDark.value = !event.target.checked
            setType(isDark.value ? TypeThemeEnum.DARK : TypeThemeEnum.LIGHT);
        }

        return {
            PREFIX,
            isDark,
            change
        }
    }
})