import { defineAsyncComponent, defineComponent } from "vue";

import { PATH_RESOURCES } from "../../../../constants/app.constant";

export default defineComponent({
    components: {
        CmpHeaderOptionAvatar: defineAsyncComponent(
            () => import("./cmp-header-option-avatar/cmp-header-option-avatar.vue")
        ),
        CmpHeaderOptionGrid: defineAsyncComponent(
            () => import("./cmp-header-option-grid/cmp-header-option-grid.vue")
        ),
        CmpHeaderOptionTheme: defineAsyncComponent(
            () => import("./cmp-header-option-theme/cmp-header-option-theme.vue")
        ),
        CmpHeaderOptionNotification: defineAsyncComponent(
            () => import("./cmp-header-option-notification/cmp-header-option-notification.vue")
        )
    },
    setup() {
        const PREFIX = "cmp-header-options";

        return {
            PREFIX,
            PATH_RESOURCES
        }
    }
})