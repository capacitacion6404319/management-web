import { defineAsyncComponent, defineComponent } from "vue";

export default defineComponent({
    components: {
        CmpOptionDropdown: defineAsyncComponent(
            () => import("../../../cmp-option-dropdown/cmp-option-dropdown.vue")
        )
    },
    setup() {
        const PREFIX = "cmp-header-option-grid";

        return {
            PREFIX
        }
    }
})