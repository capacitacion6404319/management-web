import { defineAsyncComponent, defineComponent } from "vue";

import { translate } from "../../../../../utils/language.util";

export default defineComponent({
    components: {
        CmpOptionDropdown: defineAsyncComponent(
            () => import("../../../cmp-option-dropdown/cmp-option-dropdown.vue")
        )
    },
    setup() {
        const PREFIX = "cmp-header-option-notificacion";

        return {
            PREFIX,
            translate
        }
    }
})