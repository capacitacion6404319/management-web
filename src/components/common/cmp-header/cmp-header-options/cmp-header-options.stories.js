import CmpHeaderOptions from "./cmp-header-options.vue";

import { PLANTILLA_HTML } from "../../../../constants/storybook.constant";

const TITULO = `CMP-HEADER-OPTIONS`;

const SUMMARY = `
    El componente <strong>${TITULO}</strong> renderiza la barra de opciones del aplicativo, en él se definen las siguientes <strong>propiedades</strong>:`;

const PROPS = `

`;

const COMPONENT_BASE = `
<div style="width: 100px; margin-top: 20px;">
    <cmp-header-options v-bind="args"></cmp-header-options>
</div>`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-01-30][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'Components/Common/CmpHeader/CmpHeaderOptions',
    component: CmpHeaderOptions,
}

const TemplateBase = args => ({
    components: { CmpHeaderOptions },
    setup() {
        return { args }
    },
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT_BASE}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const Base = TemplateBase.bind({});
Base.args = {

}