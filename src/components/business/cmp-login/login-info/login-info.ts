import { defineComponent } from "vue";

import { translate } from "../../../../utils/language.util";

import { PATH_RESOURCES } from "../../../../constants/app.constant";

export default defineComponent({
    setup() {
        const PREFIX = "login-info";
        return {
            PREFIX,
            PATH_RESOURCES,
            translate
        }
    }
})