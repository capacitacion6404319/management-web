import LoginInfo from "./login-info.vue";

export default {
    title: 'Components/Business/CmpLogin/LoginInfo',
    component: LoginInfo,
}

const TemplateBase = args => ({
    components: { LoginInfo },
    setup() {
        return { args }
    },
    template: `<div style="margin: auto;"><login-info v-bind="args"></login-info></div>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}