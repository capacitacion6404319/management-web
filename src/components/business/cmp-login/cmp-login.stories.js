import CmpLogin from "./cmp-login.vue";

export default {
    title: 'Components/Business/CmpLogin',
    component: CmpLogin,
}

const TemplateBase = args => ({
    components: { CmpLogin },
    setup() {
        return { args }
    },
    template: `<cmp-login v-bind="args"></cmp-login>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}