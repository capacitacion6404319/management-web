import { defineComponent, defineAsyncComponent } from "vue";

export default defineComponent({
  components: {
    LoginElipse: defineAsyncComponent(
      () => import(/* webpackChunkName: "cmp-login" */"./login-elipse/login-elipse.vue")
    ),
    LoginForm: defineAsyncComponent(
      () => import(/* webpackChunkName: "cmp-login" */"./login-form/login-form.vue")
    ),
    LoginInfo: defineAsyncComponent(
      () => import(/* webpackChunkName: "cmp-login" */"./login-info/login-info.vue")
    ),
  },
  setup() {
    const PREFIX = "cmp-login";
    return {
      PREFIX
    }
  }
});