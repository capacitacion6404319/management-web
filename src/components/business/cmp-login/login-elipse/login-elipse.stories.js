import LoginElipse from "./login-elipse.vue";

export default {
    title: 'Components/Business/CmpLogin/LoginElipse',
    component: LoginElipse,
}

const TemplateBase = args => ({
    components: { LoginElipse },
    setup() {
        return { args }
    },
    template: `<div style="background-color:#F92B60; height: 100vh"><login-elipse v-bind="args"></login-elipse></div>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}