import { defineComponent } from "vue";

export default defineComponent({
    setup() {
        const PREFIX = "login-elipse";
        return {
            PREFIX
        }
    }
})