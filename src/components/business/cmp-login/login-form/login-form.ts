import {defineComponent, reactive, onUnmounted, watchEffect, inject, onMounted} from "vue";
import { useRouter } from "vue-router";
import useVuelidate from "@vuelidate/core";

import useLoginForm from "../../../../composables/business/cmp-login/login-form.composable";
import { validatorUser } from "../../../../validators/business/cmp-login/login-form.validator";

import { translate } from "../../../../utils/language.util";
import VButtonLogin from "../../../base/v-button-login/v-button-login.vue";
import VInputLogin from "../../../base/v-input-login/v-input-login.vue";

export default defineComponent({
    components: {
        VInputLogin,
        VButtonLogin,
    },
    setup() {
        const PREFIX = "login-form";

        const overload: any = inject("$cmp-overload")

        const user = reactive({
            "Username": "",
            "Password": ""
        });

        const router = useRouter();
        const v$ = useVuelidate(validatorUser, user);
        const { loginUser, isUserInvalid, resetValues,isTokenValid,checkSession } = useLoginForm();

        const userInvalid = isUserInvalid();
        const tokenValid = isTokenValid();

        const login = async() => {
            v$.value.$validate();
            if (!v$.value.$error) {
                overload.show();
                await loginUser(user);
            }
        }
        onMounted(async()=>{
            await checkSession();
        })

        onUnmounted(() => {
            resetValues();
        })

        watchEffect(async() => {
            userInvalid.value;
            if (!tokenValid.value) {
                overload.hide();
            } else if(tokenValid.value && !userInvalid.value) {
                overload.hide();
                await router.push("/home");
            }
        })

        return {
            PREFIX,
            user,
            userInvalid,
            v$,
            login,
            translate
        }
    }
});