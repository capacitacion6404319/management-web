import LoginForm from "./login-form.vue";

export default {
    title: 'Components/Business/CmpLogin/LoginForm',
    component: LoginForm,
}

const TemplateBase = args => ({
    components: { LoginForm },
    setup() {
        return { args }
    },
    template: `<div style="margin: auto;background-color: #ff7272"><login-form v-bind="args"></login-form></div>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}