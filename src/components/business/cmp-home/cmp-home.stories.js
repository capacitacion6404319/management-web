import CmpHome from "./cmp-home.vue";

export default {
    title: 'Components/Business/CmpHome',
    component: CmpHome,
}

const TemplateBase = args => ({
    components: { CmpHome },
    setup() {
        return { args }
    },
    template: `<cmp-home v-bind="args"></cmp-home>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}