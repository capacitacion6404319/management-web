import { defineComponent, defineAsyncComponent } from "vue";

import useLoginForm from "../../../composables/business/cmp-login/login-form.composable";
import useHomeForm from "../../../composables/business/cmp-home/home-form.composable";

export default defineComponent({
    components: {
        VButtonLogin: defineAsyncComponent(
            () => import("../../base/v-button-login/v-button-login.vue")
        ),
    },
    setup() {
        const PREFIX = "cmp-home";

        const { getUser } = useLoginForm();
        const user = getUser();

        const { getDocumentState, setDocumentState, setValueSalePerItem, getValueSalePerItem } = useHomeForm();
        const documentState = getDocumentState();
        const valueSalePerItem = getValueSalePerItem();

        const executeRule = () => {
            const params = {
                idEstadoComprobante: 17,
                idTipoDocumento: 2015
            };
              
            setDocumentState(params);
        }

        const params = {
            isFree: false, 
            affectsTaxBase: false,
            amountUnitPerItem: 12,
            valueUnitPerItem: 56,
            amountBaseOfCharge: 12,
            chargeFactor: 45,
            priceUnitPerItem: 12 
        };

        const calculate = () => {              
            setValueSalePerItem(params);
        }

        return {
            PREFIX,
            user,
            documentState,
            valueSalePerItem,
            params,
            executeRule,
            calculate
        };
    },
});