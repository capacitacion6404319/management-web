import { defineComponent, ref } from "vue";

export default defineComponent({
    props: {
        icon: {
            type: String,
        },
        modelValue: [String, Number],
        disabled: {
            type: Boolean,
            default: false,
        },
        invalid: {
            type: Boolean,
            default: false,
        },
        type: {
            type: String,
            default: "text",
        },
        messageAlert: {
            type: String,
            default: "text",
        },
    },
    setup(props) {
        const PREFIJO = "v-input-login";

        const refInputPassword: any = ref(null);

        const showPassword = () => {
            if (!props.disabled) {
                refInputPassword.value.type =
                    refInputPassword.value.type == "password" ? "text" : "password";
            }
        };

        return {
            PREFIJO,
            refInputPassword,
            showPassword
        };
    },
});