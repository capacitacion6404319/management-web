import VInputLogin from "./v-input-login.vue";

import { PLANTILLA_HTML } from "../../../constants/storybook.constant";

const TITULO = `V-INPUT-LOGIN`;

const SUMMARY = `
    El componente <strong>V-INPUT-LOGIN</strong> renderiza un input para el ingreso de datos, en él se definen las siguientes <strong>propiedades</strong>:`;

const PROPS = `
    <li><strong>placeholder</strong>: Placeholder del input</li>
    <li><strong>invalid</strong>: Flag que indica si el input tiene la clase invalid</li>
`;

const COMPONENT_BASE = `
<div style="width: 500px; margin-top: 20px;">
    <v-input-login v-bind="args"></v-input-login>
</div>`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-01-30][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'Components/Base/VInputLogin',
    component: VInputLogin,
}

const TemplateBase = args => ({
    components: { VInputLogin },
    setup() {
        return { args }
    },
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT_BASE}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const InputBase = TemplateBase.bind({});
InputBase.args = {
    "placeholder": "Usuario",
    "icon": "../../../../resources/icons/user.svg",
    "messageAlert": "Usuario inválido"
}

export const InputInvalid = TemplateBase.bind({});
InputInvalid.args = {
    "placeholder": "Usuario",
    "icon": "../../../../resources/icons/user.svg",
    "messageAlert": "Usuario inválido",
    "invalid": true
}

export const PasswordBase = TemplateBase.bind({});
PasswordBase.args = {
    "placeholder": "Contraseña",
    "icon": "../../../../resources/icons/key.svg",
    "type": "password",
}

export const PasswordInvalid = TemplateBase.bind({});
PasswordInvalid.args = {
    "placeholder": "Contraseña",
    "icon": "../../../../resources/icons/key.svg",
    "messageAlert": "Contraseña inválida",
    "type": "password",
    "invalid": true
}