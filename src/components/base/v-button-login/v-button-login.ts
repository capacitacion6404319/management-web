import { defineComponent } from "vue";

export default defineComponent({
    setup() {
        const PREFIJO = "v-button-login";
        return {
            PREFIJO
        }
    }
})