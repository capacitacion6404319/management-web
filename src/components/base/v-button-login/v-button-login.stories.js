import VButtonLogin from "./v-button-login.vue";

import { PLANTILLA_HTML } from "../../../constants/storybook.constant";

const TITULO = `V-BUTTON-LOGIN`;

const SUMMARY = `
    El componente <strong>V-BUTTON-LOGIN</strong> renderiza un botón para el login al sistema, en él se definen las siguientes <strong>propiedades</strong>:`;

const PROPS = `

`;

const COMPONENT_BASE = `
<div style="width: 500px; margin-top: 20px;">
    <v-button-login v-bind="args">Ingresar</v-button-login>
</div>`;

const AUTOR = `Luis Amat`;

const HISTORY = `
    <span>[2024-01-30][Luis Amat] Creación del componente</span>
`;

export default {
    title: 'Components/Base/VButtonLogin',
    component: VButtonLogin,
}

const TemplateBase = args => ({
    components: { VButtonLogin },
    setup() {
        return { args }
    },
    template: PLANTILLA_HTML.replace("$$TITULO$$", `${TITULO}`).replace("$$SUMMARY$$", `${SUMMARY}`).replace("$$PROPS$$", `${PROPS}`).replace("$$COMPONENT$$", `${COMPONENT_BASE}`).replace("$$AUTOR$$", `${AUTOR}`).replace("$$HISTORY$$", `${HISTORY}`)
})

export const Base = TemplateBase.bind({});
Base.args = {

}