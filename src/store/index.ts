import { createStore } from 'vuex'

import homeFormStore from "./components/business/cmp-home/home-form.store";
import loginFormStore from "./components/business/cmp-login/login-form.store";

import themeStore from "./components/common/theme.store";

export default createStore({
  modules: {
    homeFormStore,
    loginFormStore,
    themeStore
  }
})