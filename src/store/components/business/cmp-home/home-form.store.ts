import { calculateValueSalePerItem, getDocumentState } from "../../../../core/accounting/infraestructure/adapter/in/accounting-documents.adapter";

export const HOME_FORM_STORE_GET_DOCUMENT_STATE = "homeFormStore/getDocumentState";
export const HOME_FORM_STORE_SET_DOCUMENT_STATE = "homeFormStore/setDocumentState";
export const HOME_FORM_STORE_GET_VALUE_SALE_PER_ITEM = "homeFormStore/getValueSalePerItem";
export const HOME_FORM_STORE_SET_VALUE_SALE_PER_ITEM = "homeFormStore/setValueSalePerItem";
export const HOME_FORM_STORE_RESET_VALUES = "homeFormStore/resetValues";

export default {
    namespaced: true,
    state: {
        documentState: false,
        valueSalePerItem: 0
    },
    mutations: {
        SET_DOCUMENT_STATE(state: any, flag: boolean) {
            state.documentState = flag;
        },
        SET_VALUE_SALE_PER_ITEM(state: any, valueSalePerItem: number) {
            state.valueSalePerItem = valueSalePerItem;
        },
        RESET_VALUES(state: any) {
            state.documentState = false;
            state.valueSalePerItem = 0;
        }
    },
    actions: {
        async setDocumentState({commit}: any, params: any) {
            try {
                const resp = await getDocumentState("document-state", params);
                commit('SET_DOCUMENT_STATE', resp);
            } catch(ex) {
                commit('SET_DOCUMENT_STATE', false);
            }            
        },
        async setValueSalePerItem({commit}: any, params: any) {
            try {
                const resp = await calculateValueSalePerItem("calculate-value-sale-per-item", params);
                commit('SET_VALUE_SALE_PER_ITEM', resp);
            } catch(ex) {
                commit('SET_VALUE_SALE_PER_ITEM', 0);
            } 
        },
        resetValues({commit}: any) {
            commit('RESET_VALUES');
        }
    },
    getters: {
        getDocumentState(state: any) {
            return state.documentState;
        },
        getValueSalePerItem(state: any) {
            return state.valueSalePerItem;
        }
    }
}