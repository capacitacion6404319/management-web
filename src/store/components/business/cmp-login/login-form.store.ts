import { login } from "../../../../core/login/infraestructure/adapter/in/service/authentication.service";
import {User, UserToken} from "../../../../core/login/domain/user.domain";
import { LocalStorageAdapter } from "../../../../core/shared/infraestructure/adapter/out/local-storage.adapter";

export const LOGIN_FORM_STORE_GET_USER = "loginFormStore/getUser";
export const LOGIN_FORM_STORE_IS_USER_INVALID = "loginFormStore/isUserInvalid";
export const LOGIN_FORM_STORE_LOGIN_USER = "loginFormStore/login";
export const LOGIN_FORM_STORE_RESET_VALUES = "loginFormStore/resetValues";
export const LOGIN_FORM_STORE_CHECK_SESSION = "loginFormStore/checkSession";
export const LOGIN_FORM_STORE_IS_TOKEN_VALID = "loginFormStore/isTokenValid";

const localStorageAdapter = new LocalStorageAdapter<UserToken>("authKey");
export default {
    namespaced: true,
    state: {
        userInvalid: false,
        user: {},
        token: null,
    },
    mutations: {
        SET_USER_INVALID(state: any, flag: boolean) {
            state.userInvalid = flag;
        },
        SET_TOKEN(state: any, token: any) {
            state.token = token;
        },
        LOGIN_USER(state: any, user: User) {
            state.user = user;
        },
        RESET_VALUES(state: any) {
            state.user = {};
            state.userInvalid = false;
            state.token=null;
        }
    },
    actions: {
        async login({commit}: any, user: User) {
            try {
                commit('RESET_VALUES', null);
                const responseUser = await login(user);
                commit('LOGIN_USER', responseUser);
                commit('SET_TOKEN',localStorageAdapter.obtenerToken());
                commit('SET_USER_INVALID', false);
            } catch(ex) {
                commit('RESET_VALUES', null);
                commit('SET_USER_INVALID', true);
            }
        },
        async resetValues({commit}: any) {
            commit('RESET_VALUES');
        },
        async checkSession({ commit }:any) {
            const token = localStorageAdapter.obtenerToken();
            if (token) {
                commit('SET_TOKEN', token);
            } else {
                commit('RESET_VALUES',null);
            }
        }
    },
    getters: {
        isUserInvalid(state: any) {
            return state.userInvalid;
        },
        getUser(state: any) {
            return state.user;
        },
        isTokenValid(state:any) {
            return !!state.token;
        }
    }
}