import i18n from './i18n.util'

export const translate = (path: string, outsideContext?: boolean, params?: any) => {
    if (outsideContext) {
        const { t } = i18n.global;
        return t(path, "es", params);
    } else {
        const { t } = i18n.global;
        return t(path, "es", params);
    }
}