import { createI18n } from 'vue-i18n'

import es from "../assets/translations/es.json";
import en from '../assets/translations/en-us.json';
import pt from '../assets/translations/pt.json';

export default createI18n({
    messages: {
        "en": en,
        "es": es,
        "pt": pt
    },
    fallbackLocale: "es"
})