export const changeProperty = (property: string, value: string) => {
    const root: any = document.querySelector(':root');
    root?.style.setProperty(property, value);
}