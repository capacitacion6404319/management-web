import { required,email } from "@vuelidate/validators";

export const validatorUser = {
    Username: { required,email },
    Password: { required }
};