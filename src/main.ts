import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import "@iconify/iconify/dist/iconify.min.js";
import "./assets/resources/fonts/line-icons-pro/line-icons-pro.css";

import { LoadPlugin } from "./plugins/common/cmp-overload.plugin"

createApp(App).use(store).use(router).use(LoadPlugin).mount('#app')