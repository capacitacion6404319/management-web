import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import VwLogin from "../views/vw-login/vw-login.vue";
import VwHome from "../views/vw-home/vw-home.vue";
import CmpHome from "../components/business/cmp-home/cmp-home.vue";
import store from "../store";
import {LOGIN_FORM_STORE_IS_TOKEN_VALID} from "../store/components/business/cmp-login/login-form.store";
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'login',
    component: VwLogin,
    meta: { requiresAuth: false }
  },
  {
    path: '/home',
    name: 'home',
    component: VwHome,
    meta: { requiresAuth: true },
    children: [
      {
        path: 'welcome',
        name: 'welcome',
        component: CmpHome
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

// Nav guard para verificar si el usuario está logueado
router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const isTokenValid = store.getters[LOGIN_FORM_STORE_IS_TOKEN_VALID];
  if (requiresAuth && !isTokenValid) {
    next('/');
  } else if (!requiresAuth && isTokenValid) {
    next('/home');
  } else {
    next();
  }
});
export default router
