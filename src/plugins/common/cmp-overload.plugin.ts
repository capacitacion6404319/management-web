import CmpOverload from "../../components/common/cmp-overload/cmp-overload.vue";
import useCmpOverload from "../../composables/common/cmp-overload/cmp-overload.composable";

const LoadPlugin = (app: any, props = {}) => {
    const instance = useCmpOverload(props);
    app.config.globalProperties.$loading = instance;
    app.provide('$cmp-overload', instance)
};

CmpOverload.install = LoadPlugin;

export default CmpOverload;
export {useCmpOverload, LoadPlugin, CmpOverload}