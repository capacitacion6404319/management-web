import VwHome from "./vw-home.vue";

export default {
    title: 'Views/VwHome',
    component: VwHome,
}

const TemplateBase = args => ({
    components: { VwHome },
    setup() {
        return { args }
    },
    template: `<vw-home v-bind="args"></vw-home>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}