import { defineComponent, defineAsyncComponent } from "vue";

export default defineComponent({
    components: {
        CmpHeader: defineAsyncComponent(() =>
            import('../../components/common/cmp-header/cmp-header.vue')
        )
    }
})