import { defineComponent, defineAsyncComponent } from "vue";

export default defineComponent({
    components: {
        CmpLogin: defineAsyncComponent(() =>
            import('../../components/business/cmp-login/cmp-login.vue')
        )
    }
})