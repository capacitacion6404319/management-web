import VwLogin from "./vw-login.vue";

export default {
    title: 'Views/VwLogin',
    component: VwLogin,
}

const TemplateBase = args => ({
    components: { VwLogin },
    setup() {
        return { args }
    },
    template: `<vw-login v-bind="args"></vw-login>`
})

export const Base = TemplateBase.bind({});
Base.args = {

}