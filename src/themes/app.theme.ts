import { changeProperty } from "../utils/theme.util";

import { TypeThemeEnum } from "../enums/type-theme.enum";

export const changeThemeApp = (type: number) => {
    if (type === TypeThemeEnum.LIGHT) {

        changeProperty("--body-background-color", '#fff');

    } else if (type === TypeThemeEnum.DARK) {

        changeProperty("--body-background-color", 'hsl(240 4% 24%)');

    }
}