import { changeProperty } from "../../utils/theme.util";

import { TypeThemeEnum } from "../../enums/type-theme.enum";

export const changeThemeOptionDropdown = (type: number) => {
    if (type === TypeThemeEnum.LIGHT) {

        changeProperty("--cmp-dropdown-menu-border-color", '#ededed');
        changeProperty("--cmp-dropdown-menu-background-color", '#fff');

        changeProperty("--cmp-header-option-notification-heading-border-bottom-color", "#eee");
        changeProperty("--cmp-header-option-notification-heading-h-left-color", "#a2a5b9");
        changeProperty("--cmp-header-option-notification-heading-r-left-color", "#41b883");
        changeProperty("--cmp-header-option-notification-notification-list-notification-item-user-content-user-info-color", "#283252");
        changeProperty("--cmp-header-option-notification-notification-list-notification-item-user-content-time-color", "#a2a5b9");

        changeProperty("--cmp-header-option-avatar-heading-background", "#fafafa");
        changeProperty("--cmp-header-option-avatar-heading-avatar-background-color", "#ededed");
        changeProperty("--cmp-header-option-avatar-heading-meta-span1-color", "#283252");
        changeProperty("--cmp-header-option-avatar-heading-meta-span2-color", "#a2a5b9");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-hover-background", "whitesmoke");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-icon-hover-color", "#41b883");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-meta-span1-color", "#283252");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-meta-span2-color", "#a2a5b9");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-divider-background-color", "#ededed");

    } else if (type === TypeThemeEnum.DARK) {

        changeProperty("--cmp-dropdown-menu-border-color", 'hsl(240 4% 22%)');
        changeProperty("--cmp-dropdown-menu-background-color", '#222225');

        changeProperty("--cmp-header-option-notification-heading-border-bottom-color", "hsl(240 4% 22%)");
        changeProperty("--cmp-header-option-notification-heading-h-left-color", "#a2a5b9");
        changeProperty("--cmp-header-option-notification-heading-r-left-color", "#2aac8e");
        changeProperty("--cmp-header-option-notification-notification-list-notification-item-user-content-user-info-color", "#a9a9b2");
        changeProperty("--cmp-header-option-notification-notification-list-notification-item-user-content-time-color", "#a2a5b9");

        changeProperty("--cmp-header-option-avatar-heading-background", "#27272a");
        changeProperty("--cmp-header-option-avatar-heading-avatar-background-color", "#545454");
        changeProperty("--cmp-header-option-avatar-heading-meta-span1-color", "#a9a9b2");
        changeProperty("--cmp-header-option-avatar-heading-meta-span2-color", "#a2a5b9");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-hover-background", "#2f2f32");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-icon-hover-color", "#41b883");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-meta-span1-color", "#a9a9b2");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-item-meta-span2-color", "#a2a5b9");
        changeProperty("--cmp-header-option-avatar-item-list-dropdown-divider-background-color", "hsl(240 4% 19%)");
    }
}