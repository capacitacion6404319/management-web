import { changeProperty } from "../../utils/theme.util";

import { TypeThemeEnum } from "../../enums/type-theme.enum";

export const changeThemeHeader = (type: number) => {
    if (type === TypeThemeEnum.LIGHT) {

        changeProperty("--cmp-header-background-color", '#fff');
        changeProperty("--cmp-header-border-bottom-color", "#ededed");

        changeProperty("--cmp-header-summary-logo-summary-summary-title-color", '#283253');
        changeProperty("--cmp-header-summary-logo-summary-separator-border-left-color", '#999');

        changeProperty("--cmp-header-option-hover-color", '#f92b60');
        changeProperty("--cmp-header-option-content-icon-color", '#f92b60');

        changeProperty("--cmp-option-option-base-content-shadow-select-background-color", '#ededed');
        changeProperty("--cmp-option-option-base-content-shadow-opt-color", '#a2a5b9');
        changeProperty("--cmp-option-option-base-content-shadow-opt-hover-color", 'rgba(0, 0, 0, 0.06)');
        changeProperty("--cmp-option-option-base-content-shadow-opt-select-background-color", '#f2f2f2');
        changeProperty("--cmp-option-option-base-content-shadow-opt-select-hover-color", '#fff');
        changeProperty("--cmp-option-option-base-new-indicator-background", '#e62965');

        changeProperty("--cmp-sub-navs-background-color","#fff");
        changeProperty("--cmp-sub-navs-border-color","#e5e5e5");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-color","#cecece");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-hover-background","#f7f7f7");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-hover-color","#b1b3c4");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-active-background","#fff");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-active-border-color","#e5e5e5");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-active-color","#41b883");
        changeProperty("--cmp-sub-navs-container-tab-content-tab-content-inner-center-columns-column-column-heading-color","#283252");
        changeProperty("--cmp-sub-navs-container-tab-content-tab-content-inner-center-columns-column-ul-li-hover-color","#41b883");
        changeProperty("--cmp-sub-navs-container-tab-content-tab-content-inner-center-columns-column-border-color","#e5e5e5");
        changeProperty("","");


    } else if (type === TypeThemeEnum.DARK) {

        changeProperty("--cmp-header-background-color", 'hsl(240 4% 20%)');
        changeProperty("--cmp-header-border-bottom-color", "hsl(240 4% 24%)");

        changeProperty("--cmp-header-summary-logo-summary-summary-title-color", '#a9a9b2');
        changeProperty("--cmp-header-summary-logo-summary-separator-border-left-color", '#6f6f6f');

        changeProperty("--cmp-header-option-hover-color", '#e62864');
        changeProperty("--cmp-header-option-content-icon-color", '#e62864');

        changeProperty("--cmp-option-option-base-content-shadow-select-background-color", '#545454');
        changeProperty("--cmp-option-option-base-content-shadow-opt-color", '#a9a9b2');
        changeProperty("--cmp-option-option-base-content-shadow-opt-hover-color", 'rgb(0 0 0 / 6%)');
        changeProperty("--cmp-option-option-base-content-shadow-opt-select-background-color", '#27272a');
        changeProperty("--cmp-option-option-base-content-shadow-opt-select-hover-color", '#27272a');
        changeProperty("--cmp-option-option-base-new-indicator-background", '#e62864');

        changeProperty("--cmp-sub-navs-background-color","#222225");
        changeProperty("--cmp-sub-navs-border-color","hsl(240 4% 22%)");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-color","#a9a9b2");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-hover-background","#545454");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-hover-color","#b1b3F4");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-active-background","#222225");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-active-border-color","#2aac8e");
        changeProperty("--cmp-sub-navs-tabs-inner-tabs-ul-li-a-active-color","#2aac8e");
        changeProperty("--cmp-sub-navs-container-tab-content-tab-content-inner-center-columns-column-column-heading-color","#a9a9b2");
        changeProperty("--cmp-sub-navs-container-tab-content-tab-content-inner-center-columns-column-ul-li-hover-color","#2aac8e");
        changeProperty("--cmp-sub-navs-container-tab-content-tab-content-inner-center-columns-column-border-color","#6f6f6f");

    }
}