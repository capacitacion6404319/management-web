/*const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
})*/

const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
  transpileDependencies: true,
  chainWebpack: (config) => {
    config.plugin('copy-webpack-plugin').use(CopyWebpackPlugin, [
      {
        patterns: [
          {
            from: 'src/assets/resources', // Ruta de tus imágenes fuente
            to: 'resources', // Ruta de destino en la carpeta de salida (dist/)
          },
          {
            from: 'robots.txt', // Ruta de tus imágenes fuente
            to: 'robots.txt', // Ruta de destino en la carpeta de salida (dist/)
          },
          {
            from: 'manifest.json', // Ruta de tus imágenes fuente
            to: 'manifest.json', // Ruta de destino en la carpeta de salida (dist/)
          },
        ],
      },
    ]);
    config.module
    .rule('js')
    .use('babel-loader')
    .loader('babel-loader')
    .tap(options => {
      // Añadir configuración específica de Babel
      options.presets = [
        [
          '@babel/preset-env',
          {
            useBuiltIns: 'entry',  // Determina qué polyfills cargar basándose en el código fuente
            corejs: 3,             // Versión de core-js
            targets: '> 0.25%, not dead, not IE 11',  // Navegadores objetivo
            modules: false,
          },
        ],
      ];

      return options;
    });    
  },
  configureWebpack: {
    mode: 'development',
    devtool: false,
    optimization: {
      minimize: true,
      minimizer: [
        new CssMinimizerPlugin(),
        new TerserPlugin({
          terserOptions: {
            ecma: 2015, // Otra opción es utilizar ecma: 2018 según tus necesidades
            mangle: true,
            output: {
              comments: false,
            },
            compress: {
              drop_console: true, // Elimina console.log y similares
              unused: true, // Elimina código no utilizado
            },
          },
          include: [
            /vue-i18n\.mjs/,
            /message-compiler\.esm-browser\.js/,
            /eventemitter2\.js/,
            /runtime-core\.esm-bundler\.js/,
            /index-browser-esm\.js/
          ]
        })
      ],
      splitChunks: {
        chunks: 'all',
        minSize: 15000,
        maxSize: 250000,
        maxAsyncRequests: 30,
        maxInitialRequests: 30,
        enforceSizeThreshold: 50000,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            name: 'vendor',
            chunks: 'all',
          },
          commons: {
            test: /[\\/]node_modules[\\/]/,
            name: "vendor",
            chunks: "all",
          },
        },
      },      
    },
    module: {
      rules: [
        {
          test: /\.(png|jpg|jpeg|gif|svg)$/i,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[hash].[ext]',
                outputPath: 'resources/'
              },
            },
          ],
        },
        {
          test: /\.m?js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                [
                  '@babel/preset-env',
                  {
                    targets: '> 0.25%, not dead',
                    modules: false,
                  },
                ],
              ],
            },
          }
        }
      ]
    }
  }
};